package de.wealthpilot.firstleague;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.wealthpilot.firstleague.dto.FootballLeagueDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Paths;

@Slf4j
@Component
public class JSONLeagueParser {


    public FootballLeagueDto parseLeagueJsonFromResources(String filename) {
        FootballLeagueDto leagueDto = new FootballLeagueDto();
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource(filename);
        try {
            InputStream inputStream = resource.getInputStream();
            leagueDto = mapper.readValue(inputStream, FootballLeagueDto.class);
        } catch (IOException e) {
            log.error("Can't parse JSON file");
        }
        return leagueDto;
    }
}
