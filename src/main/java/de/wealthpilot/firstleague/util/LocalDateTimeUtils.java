package de.wealthpilot.firstleague.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public final class LocalDateTimeUtils {
    public static LocalDateTime convertStringToLocalDateTime(String dateStr, String format) {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern(format)
                .toFormatter();
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, formatter);
        return localDateTime;
    }

    public static String convertLocalDateTimeToString(LocalDateTime ldt, String format) {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern(format)
                .toFormatter();
        return ldt.format(formatter);
    }
}
