package de.wealthpilot.firstleague;

import de.wealthpilot.firstleague.dto.FootballLeagueDto;
import de.wealthpilot.firstleague.dto.FootballMatchDto;
import de.wealthpilot.firstleague.dto.FootballTeamDto;
import de.wealthpilot.firstleague.util.LocalDateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

@Slf4j
@Component
public class LeaguePlanGenerator {

    @Autowired
    JSONLeagueParser jsonLeagueParser;

    public void generatePlan(String filename, LocalDateTime startDate, boolean singleMatchOnSaturday) {
        FootballLeagueDto footballLeagueDto = jsonLeagueParser.parseLeagueJsonFromResources(filename);

        List<FootballTeamDto> teams = footballLeagueDto.getTeams();
        if (teams == null || teams.size() < 2) {
            log.error("There is no or just one team that's why generation of schedule is not possible");
            return;
        }


        List<FootballMatchDto> firstRoundMatches = new ArrayList<>();
        List<FootballMatchDto> secondRoundMatches = new ArrayList<>();


        if (singleMatchOnSaturday) {
            planSingleMatchOnDay(teams, startDate, firstRoundMatches, secondRoundMatches, DayOfWeek.SATURDAY);
        } else {
            planMultipleMatchesOnDay(teams, startDate, firstRoundMatches, secondRoundMatches, DayOfWeek.SATURDAY);
        }

        displayMatchPlan(firstRoundMatches);
        log.info("-------------------------- SECOND ROUND --------------------------");
        displayMatchPlan(secondRoundMatches);
    }

    private void planSingleMatchOnDay(List<FootballTeamDto> teams, LocalDateTime ldtForFirstRd, List<FootballMatchDto> firstRoundMatches, List<FootballMatchDto> secondRoundMatches, DayOfWeek dayOfWeek) {
        LocalDateTime ldtForSecondRd = getLocalDateTimeForSecondRd(ldtForFirstRd, teams.size());
        for (int i = 0; i < teams.size(); i++) {
            for (int j = i + 1; j < teams.size(); j++) {
                ldtForFirstRd = ldtForFirstRd.with(TemporalAdjusters.next(dayOfWeek));
                FootballMatchDto firstRoundMatchDto = FootballMatchDto.builder()
                        .homeTeam(teams.get(i))
                        .awayTeam(teams.get(j))
                        .date(ldtForFirstRd)
                        .build();
                firstRoundMatches.add(firstRoundMatchDto);
                ldtForSecondRd = ldtForSecondRd.with(TemporalAdjusters.next(dayOfWeek));
                FootballMatchDto secondRoundMatchDto = FootballMatchDto.builder()
                        .homeTeam(teams.get(j))
                        .awayTeam(teams.get(i))
                        .date(ldtForSecondRd)
                        .build();
                secondRoundMatches.add(secondRoundMatchDto);
            }
        }
    }


    static void planMultipleMatchesOnDay(List<FootballTeamDto> teams, LocalDateTime startDate, List<FootballMatchDto> firstRoundMatches, List<FootballMatchDto> secondRoundMatches, DayOfWeek dayOfWeek) {
        int teamsCount = teams.size();
        if (teamsCount%2 != 0) {
            teams.add(new FootballTeamDto());
        }
        int numOfWeeks = teamsCount-1;
        int halfSize = teamsCount/2;
        List<FootballTeamDto> cyclicTeams = new ArrayList<>();
        cyclicTeams.addAll(teams);
        cyclicTeams.remove(0);
        int teamsSize = cyclicTeams.size();
        for (int week = 0; week<numOfWeeks; week++) {
            int teamIdx = week%teamsSize;
            startDate = startDate.with(TemporalAdjusters.next(dayOfWeek));
            FootballMatchDto match = FootballMatchDto.builder().homeTeam(cyclicTeams.get(teamIdx)).awayTeam(teams.get(0)).date(startDate).build();
            firstRoundMatches.add(match);
            for (int idx =1; idx<halfSize; idx++) {
                int firstTeam = (week + idx) % teamsSize;
                int secondTeam = (week  + teamsSize - idx) % teamsSize;
                FootballMatchDto footballMatchDto = FootballMatchDto.builder().homeTeam(cyclicTeams.get(firstTeam)).awayTeam(cyclicTeams.get(secondTeam)).date(startDate).build();
                firstRoundMatches.add(footballMatchDto);
            }
        }

        for (FootballMatchDto matchDto : firstRoundMatches) {
            FootballMatchDto secondLegMatch = FootballMatchDto.builder()
                    .homeTeam(matchDto.getAwayTeam())
                    .awayTeam(matchDto.getHomeTeam())
                    .date(matchDto.getDate().plusWeeks(3)).build();
            secondRoundMatches.add(secondLegMatch);
        }
    }

    private void displayMatchPlan(List<FootballMatchDto> matches) {
        for (FootballMatchDto match : matches) {
            String dateStr = LocalDateTimeUtils.convertLocalDateTimeToString(match.getDate(), "dd.MM.yyyy");
            log.info(dateStr + " " + match.getHomeTeam().getName() + " " + match.getAwayTeam().getName());
        }
    }

    private LocalDateTime getLocalDateTimeForSecondRd(LocalDateTime localDateTimeOfFirstRound, int numberOfTeams) {
        int matchCount = ((numberOfTeams-1) * numberOfTeams)/2;

        LocalDateTime localDateForSecondRd = localDateTimeOfFirstRound
                .with(TemporalAdjusters.next(DayOfWeek.SATURDAY))
                .plusWeeks((matchCount-1) + 2);
        log.trace("Second round starting date: " + localDateForSecondRd);
        return localDateForSecondRd;
    }
}
