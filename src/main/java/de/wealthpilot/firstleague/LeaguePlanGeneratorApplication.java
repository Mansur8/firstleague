package de.wealthpilot.firstleague;

import de.wealthpilot.firstleague.util.LocalDateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Date;

@Slf4j
@SpringBootApplication
public class LeaguePlanGeneratorApplication implements CommandLineRunner {

	@Value("${league.filename}")
	private String leagueInformationFileName;
	@Value("${league.startDateTime}")
	private String leagueStartDate;

	@Autowired
	LeaguePlanGenerator generator;

	public static void main(String[] args) {
		SpringApplication.run(LeaguePlanGeneratorApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		log.info(leagueInformationFileName + " " + leagueStartDate);
		log.info("--------------------------------------SINGLE MATCH ON SATURDAY---------------------------------");
		generator.generatePlan(leagueInformationFileName,
				LocalDateTimeUtils.convertStringToLocalDateTime(leagueStartDate, "yyyy-MM-dd HH:mm:ss"),
				true);
		log.info("--------------------------------------SEVERAL MATCHES ON SATURDAY---------------------------------");
		generator.generatePlan(leagueInformationFileName,
				LocalDateTimeUtils.convertStringToLocalDateTime(leagueStartDate, "yyyy-MM-dd HH:mm:ss"),
				false);
	}
}
