package de.wealthpilot.firstleague.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class FootballLeagueDto {
    @JsonProperty(value = "league")
    private String name;
    @JsonProperty(value = "country")
    private String country;
    @JsonProperty(value = "teams")
    private List<FootballTeamDto> teams;

}
