package de.wealthpilot.firstleague.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
public class FootballTeamDto {
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "founding_date")
    private String foundingYear;
}
