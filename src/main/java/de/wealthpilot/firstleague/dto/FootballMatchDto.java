package de.wealthpilot.firstleague.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
public class FootballMatchDto {
    private FootballTeamDto homeTeam;
    private FootballTeamDto awayTeam;
    private LocalDateTime date;
}
